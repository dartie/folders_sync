echo off
SET DirAppName=Content_compare
SET pydir=%cd%

start C:\Python27\Scripts\build
cls

echo Press ENTER for Compiling Application
pause


cd C:\Python27\Scripts
pyinstaller "%pydir%\Content_compare.py"
REM pyinstaller --icon="%pydir%\icon.ico" --onefile --console "%pydir%\Content_compare.py" 

:Copy built executable
REM copy /B /Y "dist\%DirAppName%.exe" "C:\Python27\Scripts\build\%DirAppName%"

pause
:Remove useless files
cd build\%DirAppName%
del *.manifest
del *.pkg
del *.pyz
del *.toc
del *.txt

:Copy requirements in the build directory
REM copy /B /Y "%pydir%\Requirements(package)\*.*" "%cd%\"
copy /B /Y "..\..\dist\%DirAppName%\*.pyd" "\..\Build\%DirAppName%"



pause 
echo off
REM start C:\Python27\Scripts\build