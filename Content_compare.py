# -*- coding: utf-8 -*-
#
# Author: Dario Necco <dartie90@gmail.com>
#
# This script compares 2 directories (and as many as subdirectories you specify with the parameter "depth").
# In the end, it shows missing directories and missing files.
# If required (using parameter "sync"), you can sync directories.
# "syncc" parameter, ask you for each element to copy.



__version__ = '1.28.1'

"""
CHANGELOG:
1.27.1  :   Replace unicode chars, if the server FTP returns bad chars
1.26.1  :   Managed utf8 character in directory name
1.25.1  :   FIX table in Report.html. They were printed 2 elements in the same cell

"""

#DONE: manage if I go overflow with the depth, don't still try to compare (Once there is an exception managed, next for must not trying again)
#DONE: Add parameter for screen width (and set max_char_per_line=width/2)
#DONE: manage TAB SPACE (calculate 80/2 for each result. 40 chars for each of them).
#DONE: BUG!! if depth=0 there is no sync
#DONE: manage if I want avoid file which starts with . (hidden files in UNIX). See line 247
#DONE: Create help
#DONE: results in a xml file (for color)
#DONE: pulire la funzione print_list_together: eliminare variabili non utilizzate (bg e fc)
#TODO: manage when from ftp server I get weird characters ['a__Ã\xa0', 'é__Ã¨', 'è__Ã©', 'i__Ã¬', 'o__Ã²', 'u__Ã¹']
#TODO: test hidden files on Win

import os
import os.path
import filecmp
import shutil
import socket
from colorama import init
init()
from colorama import Fore, Back, Style

from sys import platform as _platform
import sys
import codecs

dirapp = os.path.dirname(sys.executable)
# I obtain the app directory
if getattr(sys, 'frozen', False):
    # frozen
    dirapp = os.path.dirname(sys.executable)
else:
    # unfrozen
    dirapp = os.path.dirname(os.path.realpath(__file__))

if _platform == "win32":
    sep_path = "\\"
else:
    sep_path = "/"
##############################################################################################
#HELP
description = """

Directories compare

USAGE:
    COMPARE <dir1> <dir2> [OPTION]

<dir1> and <dir2> can contain '.' for indicating the current directory (e.g.: .\dir1)

OPTIONS AVAILABLE:
[sync]  :   allows to sync directories, copying missing files
[syncc] :   as the previous. Ask confirmation for each copy
[hidden]:   In UNIX, shows hidden files in the results
[width] :   sets the screen width
[view1] :   prints the results with a old view

Screen Width default values:
- Windows CMD: 80
- Windows PowerShell: 120
"""

#DEBUG
#sys.argv = [sys.argv[0], 'G:\\Disco1\\FOTO\\', 'P:\\public\\FOTO\\', 'ssync']
#sys.argv = [sys.argv[0], '.\dir1', '.\dir2']

# write what to do
global dir1         # Dir 1 to compare
global dir2         # Dir 2 to compare

global screen_width # define in order to print properly the output
global depth        # depth of comparing
global tab          # define of number of space for TAB char
global sync         # option for synchronizing missing elements
global sync_confirm # option for checking before copying
global view         # option for print in a basic mode (not alligned)
global hidden_files # compare hidden files (for Unix, compare file which start with '.')
global Stop_compare # variable that stops the comparing if the end of subdirs is reached

global html_output  # variable which contains output for html file
html_output = '<!DOCTYPE HTML>\n<html>\n<head>\n<title>Compare Report</title>\n</head>\n\n<body>\n\n<TABLE border="1" cellspacing="0" cellpadding="10">\n\t<TR>'

global Output_empty  # variable which inform if there is no output (no difference between directories), in order to inform the user
Output_empty = True

global elements_only_left
global elements_only_right
elements_only_left=[]
elements_only_right=[]

if _platform == "win32":
    screen_width = 80
else:
    screen_width = 100

depth = 0
tab_char = "\t" * 2 #tab_char = "\t" * 7
sync = False
sync_confirm = False
view = 2
hidden_files = False
Stop_compare = False

def check_argvs():
    global dir1
    global dir2
    global depth
    global sync
    global sync_confirm
    global view
    global hidden_files
    global screen_width

    dir1=['']
    dir2=['']

    if (len(sys.argv) == 1):
         help()

    if (len(sys.argv) > 1):
        if sys.argv[1].lower().startswith('help') or sys.argv[1].lower().startswith('-help') or sys.argv[1].lower().startswith('--help') or sys.argv[1].lower().startswith('-h') or sys.argv[1].lower().startswith('/h'):#Help
            help()

    for n in range(len(sys.argv)):
        #Before to analyse the arg, I replace \n in \\n in order to don't confuse with CRLF
        try:
            sys.argv[n] = sys.argv[n].replace('\n', '\\n')
        except:
            pass

        if len(sys.argv) > 2:
            if ('\\' in sys.argv[1]) or ('/' in sys.argv[1]):  #the first condition is for windows (ex. c:\project), the second is for unix (ex. /home/project)
                dir1[0] = sys.argv[1]
            if ('\\' in sys.argv[2]) or ('/' in sys.argv[2]):  #the first condition is for windows (ex. c:\project), the second is for unix (ex. /home/project)
                dir2[0] = sys.argv[2]

        if n > 0:  #if there are argvs (I start from 1, because n=0 is the 'Application name')
            #assign path to variable 'path'. if not present in argvs, path = 'application path'.
            if n > 2:
                try:
                    sys.argv[n] = unicode(sys.argv[n])  # py2
                except:
                    sys.argv[n] = str(sys.argv[n])  # py3

                try:  # I use a try-catch because int does not support ".lower()" method
                    previous_arg = sys.argv[n-1].lower()
                except:
                    previous_arg = sys.argv[n-1]


                if (sys.argv[n].isnumeric()) and not (previous_arg == "width"):             #depth
                    depth = sys.argv[n]
                elif sys.argv[n].lower() == "sync":     #sync
                    sync = True
                    sync_confirm = True
                elif sys.argv[n].lower() == "ssync":    #silent sync
                    sync = True
                    sync_confirm = False
                elif sys.argv[n].lower() == "view1":    #standard print
                    view = 1
                elif sys.argv[n].lower() == "hidden":   #includes hidden files
                    hidden_files = True
                elif sys.argv[n].lower() == "width":    #set screen width
                    try: #if sys.argv[n+1] doesn't exist
                        try:
                            next_arg = unicode(sys.argv[n+1]) # py2
                        except:
                            next_arg = str(sys.argv[n+1]) # py3

                        if (next_arg).isnumeric(): #if sys.argv[n+1] is not a number
                            screen_width = int(sys.argv[n+1])
                        else:
                            print(Fore.YELLOW + "Warning: Screen width not set properly " + Style.RESET_ALL)
                    except:
                        print(Fore.YELLOW + "Warning: Screen width not set properly " + Style.RESET_ALL)
                else:
                    try:
                        sys.argv[n] = int(sys.argv[n])
                    except:
                        pass
                    if sys.argv[n] != screen_width:
                        print(Fore.YELLOW + 'Unknown argument "' + sys.argv[n] + '". Ignored.' + Style.RESET_ALL)

    #Checks
    if dir1[0] == "" or dir2[0] == "":
        sys.exit("Error: No directories indicated! Exit")


    # Convert . (dot) char in dirapp
    if dir1[0].startswith('.' + sep_path):
        dir1[0] = dir1[0].replace('.', dirapp)

    if dir2[0].startswith('.' + sep_path):
        dir2[0] = dir2[0].replace('.', dirapp)

    if not os.path.exists(dir1[0]):
        sys.exit("Error: Left directory doesn't exist! Exit")
    if not os.path.exists(dir2[0]):
        sys.exit("Error: Right directory doesn't exist! Exit")

    try:
        depth = int(depth)
    except:
        depth = 3
        print(Fore.YELLOW + 'Used default depth value (' + depth + ')' + Style.RESET_ALL)

def help():
    print(description)

def list2unicode(list):
    list_unicode = []

    for element in list:
        try:
            list_unicode.append(unicode(element))  # py2
        except:
            list_unicode.append(str(element))  # py2

    return(list_unicode)

def fix_encoding_str_unicode(string):
    string = string.replace(u'Ã\xa0', u'à')
    string = string.replace(u'Ã©', u'è')
    string = string.replace(u'Ã¨', u'é')
    string = string.replace(u'Ã¬', u'ì')
    string = string.replace(u'Ã²', u'ò')
    string = string.replace(u'Ã¹', u'ù')

    return string

def fix_encoding_str(string):
    string = string.replace('Ã\xa0', 'à')
    string = string.replace('Ã©', 'è')
    string = string.replace('Ã¨', 'é')
    string = string.replace('Ã¬', 'ì')
    string = string.replace('Ã²', 'ò')
    string = string.replace('Ã¹', 'ù')

    return string

def fix_encoding(string):
    if isinstance(string, list):
        new_list = []
        for element in string:
            if isinstance(element, str):
                new_list.append(fix_encoding_str(element))
            elif isinstance(element, unicode):
                new_list.append(fix_encoding_str_unicode(element))
        return new_list
    elif isinstance(string, str):
        new_string = fix_encoding_str(string)
        return(new_string)
    elif isinstance(string, unicode):
        new_string = fix_encoding_str_unicode(string)
        return(new_string)

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text #or whatever

def remove_suffix(text, suffix):
    if text.endswith(suffix):
        return text[:len(text)-len(suffix)]
    return text #or whatever

def remove_hidden_files(list):
    #print(list)

    for element in list:
        if element.startswith("."):
            list.remove(element)

    return(list)

def split_string_width(string, max_char_per_line):
    lap = 1
    n_char = 1
    for char in string:
        if n_char == max_char_per_line*lap:
            string = string[:(max_char_per_line*lap) + lap-1] + '\n' + string[max_char_per_line*lap + lap-1:]#"(5*lap)" is for taking next set of chars in next lap, + "lap-1" is for the shift because a new char inserted in the string (\n)
            lap = lap + 1

        n_char = n_char + 1

    #remove last '\n' char
    string = remove_suffix(string, '\n') #remove last '\n' char, in order to don't get a empty element in the list, when I split it


    #print(string)
    string = string.split("\n")
    return string
    #print(string)

def print_list_together(list1, list2, text_prefix_po="", text_suffix_po="",distance=50, text_prefix_pe='', text_suffix_pe='', text_sep=" | ", print_content='y'):
    """
    text_prefix_po (po stands for 'per output') is the text before line (composed by list1 element + list2 element) (it could be the string for the Color/Style). Usally is Foreground-Background-Style (Bright)
    text_suffix_po (po stands for 'per output') is the text after line (composed by list1 element + list2 element) (it could be the string for the Color/Style). Usually is Reset Style
    distance is used to fill list1 element, in order to keep an equal distance between elements, so a better view. It's calculated according the screen width (On Windows by default is )
    text_prefix_pe (po element for 'per element') is the text before each element of the list (for HTML TAGS)
    text_suffix_pe (po element for 'per element') is the text after each element of the list (for HTML TAGS)
    text_sep is the text used for separating the elements of each list. Default is " | "
    """
    global html_output
    text_Html = ''

    if list1 == [] and list2 == []:
        return ''

    if len(list1) > len(list2):
        max_len = len(list1)
    else:
        max_len = len(list2)

    for x in range(max_len):
        try:
            current_left_element = list1[x]
        except:
            current_left_element = ""

        try:
            current_right_element = list2[x]
        except:
            current_right_element = ""

        if print_content == 'y':
            print(text_prefix_po + (current_left_element).ljust(distance,' ') + text_sep + (current_right_element).ljust(distance,' ') + text_suffix_po)

        # Html
        text_Html += text_prefix_po
        text_Html +=  text_prefix_pe + current_left_element + text_suffix_pe + \
                      text_prefix_pe + current_right_element + text_suffix_pe

        text_Html += text_suffix_po
    return(text_Html)

def print_list(list, text_prefix="", text_suffix="", fc_text=None, bg_text=None):
    if fc_text == None:
        fc_text = ""

    if bg_text == None:
        bg_text = ""

    for element in list:
        print(text_prefix + bg_text + fc_text + element + Style.RESET_ALL + text_suffix)

def compare_dirs(directory1, directory2):
    global Output_empty
    global html_output

    try:
        directory1 = unicode(directory1)  # py2
        directory2 = unicode(directory2)  # py2
    except:
        directory1 = str(directory1)  # py3
        directory2 = str(directory2)  # py3

    #FIX UNICODE IN CASE THE SERVER RETURNS BAD CHARS
    #directory1 = fix_encoding(directory1)
    #directory2 = fix_encoding(directory2)

    compare=filecmp.dircmp(directory1, directory2) #compare directory1 and directory2

    ################# DIRS

    #I get all directories for each dir
    subdirs_directory1 = next(os.walk(directory1))[1]
    subdirs_directory2 = next(os.walk(directory2))[1]

    #FIX UNICODE IN CASE THE SERVER RETURNS BAD CHARS
    #subdirs_directory1 = fix_encoding(subdirs_directory1)
    #subdirs_directory2 = fix_encoding(subdirs_directory2)

    #debug (print dirs content (subdirs))
    #print("directory1 subdirs: " + str(subdirs_directory1))
    #print("directory2 subdirs: " + str(subdirs_directory2))

    #from the full list, I remove the items in common
    subdirs_onlyleft = set(subdirs_directory1) - set(compare.common_dirs)
    subdirs_onlyright = set(subdirs_directory2) - set(compare.common_dirs)


    #print (debug)
    #print(str(subdirs_onlyleft))
    #print(tab_char + str(subdirs_onlyright))

    ################# FILES

    #I get all files for each dir
    files_directory1 = next(os.walk(directory1))[2]
    files_directory2 = next(os.walk(directory2))[2]

    #debug (print files content (files))
    #print("directory1 files: " + str(files_directory1))
    #print("directory2 files: " + str(files_directory2))

    #from the full list, I remove the items in common
    files_onlyleft = set(files_directory1) - set(compare.common_files)
    files_onlyright = set(files_directory2) - set(compare.common_files)

    #convert set in list
    subdirs_onlyleft = list(subdirs_onlyleft)
    subdirs_onlyright = list(subdirs_onlyright)
    files_onlyleft = list(files_onlyleft)
    files_onlyright = list(files_onlyright)

    #print (debug)     
    #print(str(files_onlyleft))
    #print(tab_char + str(files_onlyright))

    #remove hidden files if they not required (on Unix systems)
    if hidden_files == False:
        subdirs_onlyleft = remove_hidden_files(subdirs_onlyleft)
        subdirs_onlyright = remove_hidden_files(subdirs_onlyright)
        files_onlyleft = remove_hidden_files(files_onlyleft)
        files_onlyright = remove_hidden_files(files_onlyright)


    #sort lists
    subdirs_onlyleft.sort()
    subdirs_onlyright.sort()
    subdirs_onlyright.sort()
    files_onlyright.sort()

    #Print merged
    if subdirs_onlyleft != [] or files_onlyleft != [] or subdirs_onlyright != [] or files_onlyright != []: #show results only if there are differences between files, directories
        Output_empty = False
        #VIEW 1
        if view == 1:
            print(compare.left)
            print_list(subdirs_onlyleft, "", "", Fore.YELLOW)
            print_list(files_onlyleft)
            print(tab_char + compare.right)
            print_list(subdirs_onlyright, tab_char, "", Fore.YELLOW)
            print_list(files_onlyright, tab_char)
            print("-" * 50)
            print(" " * 50)

        #VIEW 2
        elif view == 2: #show results only if there are differences between files, directories
            half_screen = int((screen_width-3)/2)
            print_list_together(split_string_width(compare.left, half_screen),split_string_width(compare.right, half_screen), Fore.WHITE + Back.RED + Style.BRIGHT, Style.RESET_ALL, half_screen)
            print_list_together(subdirs_onlyleft, subdirs_onlyright, Fore.YELLOW, Style.RESET_ALL, half_screen, Fore.YELLOW)
            print_list_together(files_onlyleft, files_onlyright, '', '', half_screen)
            print("-" * screen_width)
            print(" " * screen_width)

        html_output += '\n\t\t<TH><a href="' + compare.left + '">' + compare.left + '</a></TH>\n\t\t<TH><a href="' + compare.right + '">' + compare.right + '</a></TH>\n\t</TR>\n\n'
        html_output += print_list_together(subdirs_onlyleft, subdirs_onlyright, '\t<TR>\n', '\t</TR>\n', '', '\t\t<TD><font color="orange">', '</font></TD>\n', '', 'n')
        html_output += print_list_together(files_onlyleft, files_onlyright, '\t<TR>\n', '\t</TR>\n', '', '\t\t<TD>', '</TD>\n', '', 'n')
        html_output += '<TR><td bgcolor="#000000"> </td> <td bgcolor="#000000"></TR>'


    return(subdirs_onlyleft + files_onlyleft, subdirs_onlyright + files_onlyright)

def main():
    global elements_only_left
    global elements_only_right
    global Stop_compare
    global screen_width
    global sync
    global sync_confirm
    global html_output
    print('Working dir: \"' + dirapp + '\"')
    print('-----Application started v.' + __version__ + '-----')

    check_argvs()
    elements_only_left = []
    elements_only_right = []

    for x in range(depth):
        if Stop_compare == False:
            try:
                compare = filecmp.dircmp(dir1[x], dir2[x]) #compare root directories
            except:
                #reached the end of array
                Stop_compare = True
                continue

        common_dirs = compare.common_dirs #get common dirs, in order to add them in a list for comparing later
        common_dirs = list2unicode(common_dirs)
        #common_dirs = fix_encoding(common_dirs)
        for common_dir in common_dirs:
            #print(common_dirs)
            dir1.append(os.path.join(compare.left, common_dir))
            dir2.append(os.path.join(compare.right, common_dir))

        only_left = compare.left_only #get onlyleft elemets, in order to add them in a list for syncing later
        only_left = list2unicode(only_left)
        #only_left = fix_encoding(only_left)
        for only_left_element in only_left:
            if hidden_files == False and only_left_element.startswith('.'):
                continue
            elements_only_left.append(os.path.join(compare.left, only_left_element))

        only_right = compare.right_only #get onlyright elemets, in order to add them in a list for syncing later
        only_right = list2unicode(only_right)
        #only_right = fix_encoding(only_right)
        for only_right_element in only_right:
            if hidden_files == False and only_right_element.startswith('.'):
                continue
            elements_only_right.append(os.path.join(compare.right, only_right_element))



    for index in range(len(dir1)): #compare all common dirs found
        elements_only = compare_dirs(dir1[index], dir2[index])

    #For sync
    if len(dir1)== 1 and len(dir2)== 1:
        elements_only_L = elements_only[0]
        for elementL in elements_only_L:
            elements_only_left.append(os.path.join(dir1[0], elementL))

        elements_only_R = elements_only[1]
        for elementR in elements_only_R:
            elements_only_right.append(os.path.join(dir2[0], elementR))

    input_confirm = ''
    if sync == True:
        print("") # for space between results and copy log

        #Sync DIR1 --> DIR2
        for element_dir1 in elements_only_left:
            dir2_destination_copy = remove_prefix(element_dir1, dir1[0])
            dir2_destination_copy = dir2[0] + sep_path + dir2_destination_copy
            dir2_destination_copy = dir2_destination_copy.replace(sep_path*2, sep_path)
            if sync_confirm == True:
                try:
                    input_confirm = raw_input(Back.LIGHTCYAN_EX + Fore.BLACK + '\nCopy ' + Style.RESET_ALL + element_dir1 + Back.BLACK + Fore.BLUE + Style.BRIGHT + " --> " + Style.RESET_ALL + dir2_destination_copy + '?\n' + Back.LIGHTCYAN_EX + Fore.BLACK + '(y / n / ay (all yes) / an (all no))' + Style.RESET_ALL)  # py2
                except:
                    print(Back.LIGHTCYAN_EX + Fore.BLACK + '\nCopy ' + Style.RESET_ALL + element_dir1 + Back.BLACK + Fore.BLUE + Style.BRIGHT + " --> " + Style.RESET_ALL + dir2_destination_copy + '?\n' + Back.LIGHTCYAN_EX + Fore.BLACK + '(y / n / ay (all yes) / an (all no))' + Style.RESET_ALL)
                    input_confirm = input()  # py3

                if input_confirm.lower() == "ay":
                    sync_confirm = False
                elif input_confirm.lower() == "an":
                    sync = False
                    break
                elif input_confirm.lower() != "y":
                    continue
            try:
                #shutil.copy(element_dir1, dir2_destination_copy)
                print(Back.WHITE + Fore.BLACK + "\nCopied :" + Style.RESET_ALL + " " + element_dir1 + Fore.BLUE + Style.BRIGHT + " --> " + Style.RESET_ALL + dir2_destination_copy)
            except:
                print(Back.BLACK + Style.BRIGHT + Fore.RED + "\nError copying: " + " " + element_dir1 + Fore.BLUE + Style.BRIGHT + " --> " + Style.RESET_ALL + dir2_destination_copy + Style.RESET_ALL)

        #Sync DIR2 --> DIR1
        if sync == True:
            for element_dir2 in elements_only_right:
                dir1_destination_copy = remove_prefix(element_dir2, dir2[0])
                dir1_destination_copy = dir1[0] + sep_path + dir1_destination_copy
                dir1_destination_copy = dir1_destination_copy.replace(sep_path*2, sep_path)
                if sync_confirm == True:
                    try:
                        input_confirm = raw_input(Back.LIGHTCYAN_EX + Fore.BLACK + '\nCopy ' + Style.RESET_ALL + element_dir2 + Back.BLACK + Fore.BLUE + Style.BRIGHT + " --> " + Style.RESET_ALL + dir1_destination_copy + '?\n' + Back.LIGHTCYAN_EX + Fore.BLACK + '(y / n / ay (all yes) / an (all no))' + Style.RESET_ALL)  # py2
                    except:
                        print((Back.LIGHTCYAN_EX + Fore.BLACK + '\nCopy ' + Style.RESET_ALL + element_dir2 + Back.BLACK + Fore.BLUE + Style.BRIGHT + " --> " + Style.RESET_ALL + dir1_destination_copy + '?\n' + Back.LIGHTCYAN_EX + Fore.BLACK + '(y / n / ay (all yes) / an (all no))' + Style.RESET_ALL))
                        input_confirm = input()  # py3

                    if input_confirm.lower() == "ay":
                        sync_confirm = False
                    elif input_confirm.lower() == "an":
                        sync = False
                        break
                    elif input_confirm.lower() != "y":
                        continue
                try:
                    #shutil.copy(element_dir2, dir1_destination_copy)
                    print(Back.WHITE + Fore.BLACK + "\nCopied :" + Style.RESET_ALL + " " + element_dir2 + Fore.BLUE + Style.BRIGHT + " --> " + Style.RESET_ALL + dir1_destination_copy)
                except:
                    print(Back.BLACK + Style.BRIGHT + Fore.RED + "\nError copying: " + " " + element_dir2 + Fore.BLUE + Style.BRIGHT + " --> " + Style.RESET_ALL + dir1_destination_copy + Style.RESET_ALL)


    # Write HTML output
    html_output += u'\n<p></p>\n<p></p>\n\t\n</TABLE>\n</body>\n\n</html>'
    write_HTML_out = codecs.open(os.path.join(dirapp, 'Report.html'), 'w', encoding='utf-8')
    #print(html_output)
    if Output_empty == True:
        print("The diretories are equals")
        html_output = "The diretories are equals"
    write_HTML_out.write(html_output)
    write_HTML_out.close()


if __name__ == '__main__':
  main()
